# my-blog

## 安装依赖

pip install -r requirements.txt

## 安装插件

mkdir pelican-plugins

git submodule add https://github.com/getpelican/pelican-plugins pelican-plugins

## 安装主题

mkdir pelican-themes

git submodule add https://github.com/getpelican/pelican-themes.git pelican-themes

## 更新submodule

git submodule update --init --recursive

git submodule update --remote --merge --recursive

## 重新生成

pelican content -o output

## 启动开发服务器

invoke livereload

## 上传生产服务器

invoke publish