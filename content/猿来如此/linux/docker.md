Title: docker基础使用
Slug: 201912242132
Date: 2019/12/24 21:32
Category: 猿来如此
Tags: linux
author: 疾风
Summary: 环境配置太烦了,我用docker

## 入门教程:
http://www.ruanyifeng.com/blog/2018/02/docker-tutorial.html
## 国内镜像
https://www.docker-cn.com/registry-mirror
## 固定IP
https://www.jianshu.com/p/19cd3654e4a4

## 删除所有容器:
docker container rm $(docker container ls -a -q)

## 运行容器:
docker container run -itd --net bridge -p 8000:3000  koa-demo:0.0.1 /bin/bash

## 容器开源管理:portainer
https://www.portainer.io/