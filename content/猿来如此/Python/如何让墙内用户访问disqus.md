Title: 如何让墙内用户访问disqus
Slug: 201912281544
Date: 2019/12/28 15:44
Category: 猿来如此
Tags: Python
author: 疾风
Summary: disqus是国外一个专业的三方评论系统，类似已经关闭的多说和还在运营的畅言，功能各方面比较完善，但由于“长城”的原因和国内的舆论监管，导致已无法愉快的使用。
         还好有大神写了套php代码可以搭建起连接disqus和你网站之间的桥梁,然后就可以使用disqus了

### 教程地址

 github项目地址内含有详细配置:[网站设置](https://github.com/fooleap/disqus-php-api)
 
### 主要流程
* 比如我的网站是tomfield.top,要再申请一个二级域名比如说api.tomfield.top
* 服务器启动一个php服务监听80端口,server_name配置为上面申请的二级域名
* 克隆disqus-php-api项目,
* 按照readme.md做好基础配置,将api目录上传到服务器根目录
* npm install安装依赖
* 执行npm run build:prod进行webpack打包dist文件
* 将打包好的dist文件夹下的文件上传到服务器的WWW目录
