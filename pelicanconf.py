#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os
import platform
from pelican.settings import get_settings_from_file

AUTHOR = '疾风'
SITENAME = 'windstorm'
# SITEURL = 'http://127.0.0.1:8000'
# hack
if platform.system() == 'Linux':
    SITEURL = 'https://www.tomfield.top'
else:
    SITEURL = 'http://127.0.0.1:8000'

PATH = 'content'

TIMEZONE = 'Asia/Shanghai'

DEFAULT_LANG = 'zh_CN'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
LINKS = (
    ('前端TOP100', 'https://www.awesomes.cn/rank?sort=trend'),
    ('JavaScriptMDN', 'https://developer.mozilla.org/zh-CN/docs/Web/JavaScript'),
    ('Python资源大全', 'http://jobbole.github.io/awesome-python-cn/'),
    ('GITHUB趋势', 'https://github.com/trending?since=daily'),
    ('吴恩达机器学习系列课程', 'https://www.bilibili.com/video/av50747658?p=1'),
)

# Blogroll

# Social widget
SOCIAL = (('GitHub', 'https://github.com/tom-field'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
# 新增配置
LOCALE = ('chs')
USE_FOLDER_AS_CATEGORY = True
IPYNB_USE_METACELL = True
MARKUP = ('md', 'ipynb')
# Plugin
PLUGIN_PATHS = ['./pelican-plugins']
# PLUGINS = ['render_math']
PLUGINS = ['pelican-ipynb.markup', 'read_more_link', 'tipue_search']
TIPUE_SEARCH = True
DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'authors', 'archives', 'search']
TEMPLATE_PAGES = {
    'search.html': 'search.html',
}
# if you create jupyter files in the content dir, snapshots are saved with the same
# metadata. These need to be ignored.
IGNORE_FILES = [".ipynb_checkpoints"]
# themes
# THEME_STATIC_DIR = 'pelican-themes'
# 备选: tuxlite_tbs attila cebong foundation-default-colours mg pelican-bootstrap3 Responsive-Pelican simple-bootstrap
THEME = os.path.abspath('./') + '/pelican-themes/tuxlite_tbs'

STATIC_PATHS = ['assets']

EXTRA_PATH_METADATA = {
    'assets/extra/favicon.ico': {'path': 'favicon.ico'},
}

# read_more_link 配置
# This settings indicates that you want to create summary at a certain length
SUMMARY_MAX_LENGTH = 100

# This indicates what goes inside the read more link
READ_MORE_LINK = '<span>continue</span>'

# This is the format of the read more link
READ_MORE_LINK_FORMAT = '<a class="read-more" href="/{url}">{text}</a>'

# 评论系统
DISQUS_SITENAME = 'windstorm'
